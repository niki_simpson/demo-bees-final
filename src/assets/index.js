import aeFlag from "../assets/ae.png";
import bgFlag from "../assets/bg.png";
import logoBg from "../assets/logoBg.png";
import logoAe from "../assets/logo-ae.png";
import logoAeDark from "../assets/logo-ae-dark.png";
import ElementorSvg from "./svg/ElementorSvg";
import FacebookIcon from "./svg/FacebookIcon";
import InstagramIcon from "./svg/InstagramIcon";
import PhoneIcon from "./svg/PhoneIcon";
import * as Icon from "./svg/icons";
import meatPng from "./dsBuffer.bmp.png";
import tasteFarm from "./taste-farm.png";
import beeRight from "./beeRight.png";
import drippingHoney from "./dripping-honey.png";

export {
  aeFlag,
  bgFlag,
  ElementorSvg,
  FacebookIcon,
  InstagramIcon,
  PhoneIcon,
  Icon,
  meatPng,
  logoBg,
  logoAe,
  logoAeDark,
  tasteFarm,
  beeRight,
  drippingHoney,
};
