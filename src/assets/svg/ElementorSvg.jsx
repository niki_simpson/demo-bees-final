const ElementorSvg = ({ color, styled }) => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1000 100' preserveAspectRatio='none'
      fill={color}
      style={styled}
    >
      <path d='M500,98.9L0,6.1V0h1000v6.1L500,98.9z' />
    </svg>
  );
};
export default ElementorSvg;