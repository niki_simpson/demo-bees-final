import img from '../assets/shop-1.png';

//215 × 207 px

export const main = {
  bg: {
    logo: 'Щастливата ферма',
    seeMore: 'Вижте повече',
    recommend: 'Препоръчваме',
  },
  ae: {
    logo: 'Happy farm',
    seeMore: 'See more',
    recommend: 'Recommend',
  },
};

export const mainPage = {
  bg: {
    home: 'Начало',
    products: 'Продукти',
    aboutUs: 'За нас',
    contacts: 'Контакти',
    locations: 'Магазини',
  },
  ae: {
    home: 'Home',
    products: 'Products',
    aboutUs: 'About us',
    contacts: 'Contacts',
    locations: 'Locations',
  },
  to: {
    home: '/',
    products: '/products',
    contact: '/contact',
    about: '/about',
    locations: '/locations',
  },
};
export const addresses = {
  bg: {
    address1: {
      src: `${img}`,
      town: 'Стара Загора',
      street: 'Цар Симеон Велики 165',
      id: 1,
      href: 'https://www.google.com/search?q=HAPPY%20FARM%20%2F%20%D0%A9%D0%B0%D1%81%D1%82%D0%BB%D0%B8%D0%B2%D0%B0%D1%82%D0%B0%20%D1%84%D0%B5%D1%80%D0%BC%D0%B0&hl=bg&sxsrf=AJOqlzWh86lKD4jn0X8NxQNBmZ1gV_1r6A:1679660581375&source=hp&ei=IpYdZLOeMY-Nxc8PiOa0wAk&iflsig=AK50M_UAAAAAZB2kMsVoWO2No04YePMW-rPWpxqCFhL9&ved=2ahUKEwid366ix_T9AhUSRPEDHaPlD0AQvS56BAgVEAE&uact=5&oq=HAPPY+FARM+%2F+%D0%A9%D0%B0%D1%81%D1%82%D0%BB%D0%B8%D0%B2%D0%B0%D1%82%D0%B0+%D1%84%D0%B5%D1%80%D0%BC%D0%B0&gs_lcp=Cgdnd3Mtd2l6EAMyBAgjECcyBggAEBYQHjoHCCMQ6gIQJ1CeBFieBGCyCGgBcAB4AIABYogBYpIBATGYAQCgAQKgAQGwAQo&sclient=gws-wiz&tbs=lf:1,lf_ui:2&tbm=lcl&rflfq=1&num=10&rldimm=17301448000833148959&lqi=CixIQVBQWSBGQVJNIC8g0KnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsEi46fWerLmAgAhaPBAAEAEQAhADGAAYARgCGAMiKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsJIBC21lYXRfcGFja2VyqgFUEAEqLiIqaGFwcHkgZmFybSDRidCw0YHRgtC70LjQstCw0YLQsCDRhNC10YDQvNCwKBsyIBABIhw9mpM8eXE6cP64LenXJ_4y-XxQzadHNsDYOFtA&sa=X&rlst=f#rlfi=hd:;si:2909219768228931532,l,CixIQVBQWSBGQVJNIC8g0KnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsEjK2-OX9LiAgAhaPBAAEAEQAhADGAAYARgCGAMiKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsJIBDGJ1dGNoZXJfc2hvcKoBVBABKi4iKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsCgbMiAQASIcPZqTPHlxOnD-uC3p1yf-Mvl8UM2nRzbA2DhbQA;mv:[[42.4220978,25.6310428],[42.40998750000001,25.628733999999998]]',
    },
    address2: {
      src: `${img}`,
      town: 'Стара Загора',
      street: 'Цар Симеон Велики 162',
      id: 2,
      href: 'https://www.google.com/search?q=HAPPY%20FARM%20%2F%20%D0%A9%D0%B0%D1%81%D1%82%D0%BB%D0%B8%D0%B2%D0%B0%D1%82%D0%B0%20%D1%84%D0%B5%D1%80%D0%BC%D0%B0&hl=bg&sxsrf=AJOqlzWh86lKD4jn0X8NxQNBmZ1gV_1r6A:1679660581375&source=hp&ei=IpYdZLOeMY-Nxc8PiOa0wAk&iflsig=AK50M_UAAAAAZB2kMsVoWO2No04YePMW-rPWpxqCFhL9&ved=2ahUKEwid366ix_T9AhUSRPEDHaPlD0AQvS56BAgVEAE&uact=5&oq=HAPPY+FARM+%2F+%D0%A9%D0%B0%D1%81%D1%82%D0%BB%D0%B8%D0%B2%D0%B0%D1%82%D0%B0+%D1%84%D0%B5%D1%80%D0%BC%D0%B0&gs_lcp=Cgdnd3Mtd2l6EAMyBAgjECcyBggAEBYQHjoHCCMQ6gIQJ1CeBFieBGCyCGgBcAB4AIABYogBYpIBATGYAQCgAQKgAQGwAQo&sclient=gws-wiz&tbs=lf:1,lf_ui:2&tbm=lcl&rflfq=1&num=10&rldimm=17301448000833148959&lqi=CixIQVBQWSBGQVJNIC8g0KnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsEi46fWerLmAgAhaPBAAEAEQAhADGAAYARgCGAMiKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsJIBC21lYXRfcGFja2VyqgFUEAEqLiIqaGFwcHkgZmFybSDRidCw0YHRgtC70LjQstCw0YLQsCDRhNC10YDQvNCwKBsyIBABIhw9mpM8eXE6cP64LenXJ_4y-XxQzadHNsDYOFtA&sa=X&rlst=f#rlfi=hd:;si:17301448000833148959,l,CixIQVBQWSBGQVJNIC8g0KnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsEi46fWerLmAgAhaPBAAEAEQAhADGAAYARgCGAMiKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsJIBC21lYXRfcGFja2VyqgFUEAEqLiIqaGFwcHkgZmFybSDRidCw0YHRgtC70LjQstCw0YLQsCDRhNC10YDQvNCwKBsyIBABIhw9mpM8eXE6cP64LenXJ_4y-XxQzadHNsDYOFtA;mv:[[42.4220978,25.6310428],[42.40998750000001,25.628733999999998]];tbs:lrf:!1m4!1u3!2m2!3m1!1e1!1m4!1u2!2m2!2m1!1e1!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:2',
    },
    address3: {
      src: `${img}`,
      town: 'Стара Загора',
      street: 'Цар Симеон Велики 161',
      id: 3,
      href: 'https://www.google.com/search?q=HAPPY%20FARM%20%2F%20%D0%A9%D0%B0%D1%81%D1%82%D0%BB%D0%B8%D0%B2%D0%B0%D1%82%D0%B0%20%D1%84%D0%B5%D1%80%D0%BC%D0%B0&hl=bg&sxsrf=AJOqlzWh86lKD4jn0X8NxQNBmZ1gV_1r6A:1679660581375&source=hp&ei=IpYdZLOeMY-Nxc8PiOa0wAk&iflsig=AK50M_UAAAAAZB2kMsVoWO2No04YePMW-rPWpxqCFhL9&ved=2ahUKEwid366ix_T9AhUSRPEDHaPlD0AQvS56BAgVEAE&uact=5&oq=HAPPY+FARM+%2F+%D0%A9%D0%B0%D1%81%D1%82%D0%BB%D0%B8%D0%B2%D0%B0%D1%82%D0%B0+%D1%84%D0%B5%D1%80%D0%BC%D0%B0&gs_lcp=Cgdnd3Mtd2l6EAMyBAgjECcyBggAEBYQHjoHCCMQ6gIQJ1CeBFieBGCyCGgBcAB4AIABYogBYpIBATGYAQCgAQKgAQGwAQo&sclient=gws-wiz&tbs=lf:1,lf_ui:2&tbm=lcl&rflfq=1&num=10&rldimm=17301448000833148959&lqi=CixIQVBQWSBGQVJNIC8g0KnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsEi46fWerLmAgAhaPBAAEAEQAhADGAAYARgCGAMiKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsJIBC21lYXRfcGFja2VyqgFUEAEqLiIqaGFwcHkgZmFybSDRidCw0YHRgtC70LjQstCw0YLQsCDRhNC10YDQvNCwKBsyIBABIhw9mpM8eXE6cP64LenXJ_4y-XxQzadHNsDYOFtA&sa=X&rlst=f#rlfi=hd:;si:17301448000833148959,l,CixIQVBQWSBGQVJNIC8g0KnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsEi46fWerLmAgAhaPBAAEAEQAhADGAAYARgCGAMiKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsJIBC21lYXRfcGFja2VyqgFUEAEqLiIqaGFwcHkgZmFybSDRidCw0YHRgtC70LjQstCw0YLQsCDRhNC10YDQvNCwKBsyIBABIhw9mpM8eXE6cP64LenXJ_4y-XxQzadHNsDYOFtA;mv:[[42.4220978,25.6310428],[42.40998750000001,25.628733999999998]];tbs:lrf:!1m4!1u3!2m2!3m1!1e1!1m4!1u2!2m2!2m1!1e1!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:2',
    },
  },
  ae: {
    address1: {
      src: `${img}`,
      town: 'Stara Zagora',
      street: 'Tsar Simeon Veliki 169',
      id: 4,
      href: 'https://www.google.com/search?q=HAPPY%20FARM%20%2F%20%D0%A9%D0%B0%D1%81%D1%82%D0%BB%D0%B8%D0%B2%D0%B0%D1%82%D0%B0%20%D1%84%D0%B5%D1%80%D0%BC%D0%B0&hl=bg&sxsrf=AJOqlzWh86lKD4jn0X8NxQNBmZ1gV_1r6A:1679660581375&source=hp&ei=IpYdZLOeMY-Nxc8PiOa0wAk&iflsig=AK50M_UAAAAAZB2kMsVoWO2No04YePMW-rPWpxqCFhL9&ved=2ahUKEwid366ix_T9AhUSRPEDHaPlD0AQvS56BAgVEAE&uact=5&oq=HAPPY+FARM+%2F+%D0%A9%D0%B0%D1%81%D1%82%D0%BB%D0%B8%D0%B2%D0%B0%D1%82%D0%B0+%D1%84%D0%B5%D1%80%D0%BC%D0%B0&gs_lcp=Cgdnd3Mtd2l6EAMyBAgjECcyBggAEBYQHjoHCCMQ6gIQJ1CeBFieBGCyCGgBcAB4AIABYogBYpIBATGYAQCgAQKgAQGwAQo&sclient=gws-wiz&tbs=lf:1,lf_ui:2&tbm=lcl&rflfq=1&num=10&rldimm=17301448000833148959&lqi=CixIQVBQWSBGQVJNIC8g0KnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsEi46fWerLmAgAhaPBAAEAEQAhADGAAYARgCGAMiKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsJIBC21lYXRfcGFja2VyqgFUEAEqLiIqaGFwcHkgZmFybSDRidCw0YHRgtC70LjQstCw0YLQsCDRhNC10YDQvNCwKBsyIBABIhw9mpM8eXE6cP64LenXJ_4y-XxQzadHNsDYOFtA&sa=X&rlst=f#rlfi=hd:;si:2909219768228931532,l,CixIQVBQWSBGQVJNIC8g0KnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsEjK2-OX9LiAgAhaPBAAEAEQAhADGAAYARgCGAMiKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsJIBDGJ1dGNoZXJfc2hvcKoBVBABKi4iKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsCgbMiAQASIcPZqTPHlxOnD-uC3p1yf-Mvl8UM2nRzbA2DhbQA;mv:[[42.4220978,25.6310428],[42.40998750000001,25.628733999999998]]',
    },
    address2: {
      src: `${img}`,
      town: 'Stara Zagora',
      street: 'Tsar Simeon Veliki 169',
      id: 5,
      href: 'https://www.google.com/search?q=HAPPY%20FARM%20%2F%20%D0%A9%D0%B0%D1%81%D1%82%D0%BB%D0%B8%D0%B2%D0%B0%D1%82%D0%B0%20%D1%84%D0%B5%D1%80%D0%BC%D0%B0&hl=bg&sxsrf=AJOqlzWh86lKD4jn0X8NxQNBmZ1gV_1r6A:1679660581375&source=hp&ei=IpYdZLOeMY-Nxc8PiOa0wAk&iflsig=AK50M_UAAAAAZB2kMsVoWO2No04YePMW-rPWpxqCFhL9&ved=2ahUKEwid366ix_T9AhUSRPEDHaPlD0AQvS56BAgVEAE&uact=5&oq=HAPPY+FARM+%2F+%D0%A9%D0%B0%D1%81%D1%82%D0%BB%D0%B8%D0%B2%D0%B0%D1%82%D0%B0+%D1%84%D0%B5%D1%80%D0%BC%D0%B0&gs_lcp=Cgdnd3Mtd2l6EAMyBAgjECcyBggAEBYQHjoHCCMQ6gIQJ1CeBFieBGCyCGgBcAB4AIABYogBYpIBATGYAQCgAQKgAQGwAQo&sclient=gws-wiz&tbs=lf:1,lf_ui:2&tbm=lcl&rflfq=1&num=10&rldimm=17301448000833148959&lqi=CixIQVBQWSBGQVJNIC8g0KnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsEi46fWerLmAgAhaPBAAEAEQAhADGAAYARgCGAMiKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsJIBC21lYXRfcGFja2VyqgFUEAEqLiIqaGFwcHkgZmFybSDRidCw0YHRgtC70LjQstCw0YLQsCDRhNC10YDQvNCwKBsyIBABIhw9mpM8eXE6cP64LenXJ_4y-XxQzadHNsDYOFtA&sa=X&rlst=f#rlfi=hd:;si:17301448000833148959,l,CixIQVBQWSBGQVJNIC8g0KnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsEi46fWerLmAgAhaPBAAEAEQAhADGAAYARgCGAMiKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsJIBC21lYXRfcGFja2VyqgFUEAEqLiIqaGFwcHkgZmFybSDRidCw0YHRgtC70LjQstCw0YLQsCDRhNC10YDQvNCwKBsyIBABIhw9mpM8eXE6cP64LenXJ_4y-XxQzadHNsDYOFtA;mv:[[42.4220978,25.6310428],[42.40998750000001,25.628733999999998]];tbs:lrf:!1m4!1u3!2m2!3m1!1e1!1m4!1u2!2m2!2m1!1e1!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:2',
    },
    address3: {
      src: `${img}`,
      town: 'Stara Zagora',
      street: 'Tsar Simeon Veliki 169',
      id: 6,
      href: 'https://www.google.com/search?q=HAPPY%20FARM%20%2F%20%D0%A9%D0%B0%D1%81%D1%82%D0%BB%D0%B8%D0%B2%D0%B0%D1%82%D0%B0%20%D1%84%D0%B5%D1%80%D0%BC%D0%B0&hl=bg&sxsrf=AJOqlzWh86lKD4jn0X8NxQNBmZ1gV_1r6A:1679660581375&source=hp&ei=IpYdZLOeMY-Nxc8PiOa0wAk&iflsig=AK50M_UAAAAAZB2kMsVoWO2No04YePMW-rPWpxqCFhL9&ved=2ahUKEwid366ix_T9AhUSRPEDHaPlD0AQvS56BAgVEAE&uact=5&oq=HAPPY+FARM+%2F+%D0%A9%D0%B0%D1%81%D1%82%D0%BB%D0%B8%D0%B2%D0%B0%D1%82%D0%B0+%D1%84%D0%B5%D1%80%D0%BC%D0%B0&gs_lcp=Cgdnd3Mtd2l6EAMyBAgjECcyBggAEBYQHjoHCCMQ6gIQJ1CeBFieBGCyCGgBcAB4AIABYogBYpIBATGYAQCgAQKgAQGwAQo&sclient=gws-wiz&tbs=lf:1,lf_ui:2&tbm=lcl&rflfq=1&num=10&rldimm=17301448000833148959&lqi=CixIQVBQWSBGQVJNIC8g0KnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsEi46fWerLmAgAhaPBAAEAEQAhADGAAYARgCGAMiKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsJIBC21lYXRfcGFja2VyqgFUEAEqLiIqaGFwcHkgZmFybSDRidCw0YHRgtC70LjQstCw0YLQsCDRhNC10YDQvNCwKBsyIBABIhw9mpM8eXE6cP64LenXJ_4y-XxQzadHNsDYOFtA&sa=X&rlst=f#rlfi=hd:;si:17301448000833148959,l,CixIQVBQWSBGQVJNIC8g0KnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsEi46fWerLmAgAhaPBAAEAEQAhADGAAYARgCGAMiKmhhcHB5IGZhcm0g0YnQsNGB0YLQu9C40LLQsNGC0LAg0YTQtdGA0LzQsJIBC21lYXRfcGFja2VyqgFUEAEqLiIqaGFwcHkgZmFybSDRidCw0YHRgtC70LjQstCw0YLQsCDRhNC10YDQvNCwKBsyIBABIhw9mpM8eXE6cP64LenXJ_4y-XxQzadHNsDYOFtA;mv:[[42.4220978,25.6310428],[42.40998750000001,25.628733999999998]];tbs:lrf:!1m4!1u3!2m2!3m1!1e1!1m4!1u2!2m2!2m1!1e1!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:2',
    },
  },
};

export const carousel = {
  firstSlide: {
    src: 'https://media.istockphoto.com/id/1310910433/photo/selection-of-assorted-raw-meat-food-for-zero-carb-carnivore-diet-uncooked-beef-steak-ground.jpg?b=1&s=170667a&w=0&k=20&c=kHDw07LONz2akPa8pPe_rhUXhoc_aCryBdgI9G2QG3g=',
    text: 'Обичаме месото. На пазара сме от 50 години. Бля бля бля'
  },
  secondSlide: {
    src: 'https://images.unsplash.com/photo-1607623814075-e51df1bdc82f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8bWVhdHxlbnwwfHwwfHw%3D&w=1000&q=80',
    text: 'Обичаме месото. На пазара сме от 50 години. Бля бля бля'
  },
  thirdSlide: {
    src: 'https://media.istockphoto.com/id/1288461867/photo/variety-of-raw-meat-steaks.jpg?s=612x612&w=0&k=20&c=hyZQzJFHD-8BQIbX-AHNR6r1PmfMRl5OKxDA_ed38DA=',
    text: 'Обичаме месото. На пазара сме от 50 години. Бля бля бля'
  },
};
