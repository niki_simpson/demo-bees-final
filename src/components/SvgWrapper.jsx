import { useEffect, useState } from "react";
import styled, { useTheme } from "styled-components";
import { FacebookIcon, Icon, InstagramIcon } from "../assets";

const SvgWrapper = ({ color }) => {
  const [call, setCall] = useState(false);
  const { isLandscape } = useTheme();
  useEffect(() => {
    // eslint-disable-next-line no-undef
    call && (window.location = "tel:+359886226429");
    return () => {
      setCall(false);
    };
  }, [call]);

  return (
    <Wrapper>
      <SvgSingleWrapper>
        <Icon.SlPhone
          onClick={() => setCall(true)}
          fill={color ? color : "black"}
          size={isLandscape ? "30" : "25"}
        />
      </SvgSingleWrapper>
      <SvgSingleWrapper>
        <FacebookIcon color={color} size={isLandscape ? "30" : "25"} />
      </SvgSingleWrapper>
      <SvgSingleWrapper>
        <InstagramIcon color={color} size={isLandscape ? "33" : "28"} />
      </SvgSingleWrapper>
    </Wrapper>
  );
};
export default SvgWrapper;
const Wrapper = styled.div`
  display: flex;
  ${({ theme }) => theme.isOnlyLandscape && "margin-right: 15%"};
  cursor: pointer;
`;

const SvgSingleWrapper = styled.div`
  margin-left: 10px;
  margin-top: 4px;
  z-index: 1;
`;
