import { Link } from "react-router-dom";
import styled, { css, keyframes, useTheme } from "styled-components";
import { mainPage } from "../../localizations/strings";
import { handleClickScroll } from "../../utils";

const ExtendedNavbar = ({ extendNavbar, language }) => {
  const { isLandscape } = useTheme();
  const label = Object.entries(mainPage[language]);

  return (
    <NavbarExtendedContainer isLandscape={isLandscape}>
      {label
        .map((e) => e)
        .map(([loc, text]) =>
          loc !== "locations" ? (
            <NavbarLinkExtended
              key={text}
              extendNavbar={extendNavbar}
              to={`/${loc}`}
            >
              {text}
            </NavbarLinkExtended>
          ) : (
            <NavbarLinkExtended
              key={text}
              extendNavbar={extendNavbar}
              onClick={handleClickScroll}
            >
              {text}
            </NavbarLinkExtended>
          )
        )}
    </NavbarExtendedContainer>
  );
};
export default ExtendedNavbar;

const NavbarExtendedContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: fit-content;
  background-color: #27442d;
  top: 11vh;
  left: 7vh;
  ${({ isLandscape }) =>
    isLandscape &&
    css`
      display: none;
    `}
`;

const slideDown = keyframes`
	0% {
		transform: translateY(-100%);
	}
	50%{
		transform: translateY(8%);
	}
	80%{
		transform: translateY(14%);
	}
  90%{
		transform: translateY(15%);
	}
	100% {
		transform: translateY(16%);
	}		
`;

const NavbarLinkExtended = styled(Link)`
  color: white;
  font-size: 1.3em;
  text-decoration: none;
  margin: 8px;
  position: relative;
  ${({ extendNavbar }) =>
    extendNavbar &&
    css`
      position: relative;
      transform: translateY(16%);
      animation-name: ${slideDown};
      animation-duration: 1200ms;
      animation-timing-function: ease;
      visibility: visible !important;
    `}
`;
