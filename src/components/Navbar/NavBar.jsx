import { useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { useAppSelector } from "../../store";
import { logoAe, drippingHoney } from "../../assets";
import SvgWrapper from "../SvgWrapper";
import ExtendedNavbar from "./ExtendedNavbar";
import { mainPage } from "../../localizations/strings";
import { PiShoppingCartLight } from "react-icons/pi";

const NavBar = () => {
  const [extendNavbar, setExtendNavbar] = useState(false);
  const { language } = useAppSelector((state) => state.languageReducer);
  const label = mainPage[language];

  return (
    <Drops>
      <NavbarContainer extendNavbar={extendNavbar}>
        <NavbarInnerContainer>
          <NavbarLinkContainer>
            <NavbarLink to="/">{label.home}</NavbarLink>
            <NavbarLink to="/products">{label.products}</NavbarLink>
            <Link style={{ textAlign: "center" }} to={"/"}>
              <Logo
                src={
                  language === "bg"
                    ? "https://png.pngtree.com/png-clipart/20230303/ourmid/pngtree-43-honey-logo-for-profile-picture-honey-free-download-png-image_6628372.png"
                    : logoAe
                }
              />
            </Link>
            <NavbarLink to="/contact">{label.contacts}</NavbarLink>
            <NavbarLink to="/about">{label.aboutUs}</NavbarLink>
          </NavbarLinkContainer>
          <OpenLinksButton onClick={() => setExtendNavbar(!extendNavbar)}>
            {extendNavbar ? <>&#10005;</> : <>&#8801;</>}
          </OpenLinksButton>
          <SvgsWrapper>
            <SvgWrapper />
          </SvgsWrapper>
          <PiShoppingCartLight size={30} />
        </NavbarInnerContainer>
        {extendNavbar && (
          <ExtendedNavbar language={language} extendNavbar={extendNavbar} />
        )}
      </NavbarContainer>
    </Drops>
  );
};

export default NavBar;

const NavbarContainer = styled.nav`
  display: flex;
  width: 100%;
  height: ${({ extendNavbar }) => (extendNavbar ? "fit-content" : "65px")};
  flex-direction: column;
  ${({ theme }) => (theme.isOnlyLandscape ? "height: 60px" : "height: 70px")}
`;

const NavbarInnerContainer = styled.div`
  display: flex;
  background-color: #f9d40d; //#f9c901;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: ${({ theme }) => (theme.isDesktop ? 70 : 60)}px;
`;

const NavbarLinkContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const NavbarLink = styled(Link)`
  color: black;
  font-size: 1.5em;
  font-weight: 500;
  text-decoration: none;
  margin: 15px;
  font-size: 1em;
  ${({ theme }) =>
    theme.isDesktop &&
    `
  margin: 30px;
  font-size: 1.5em;`}

  @media (max-width: 666px) {
    display: none;
  }
`;

const SvgsWrapper = styled.div`
  display: ${({ theme }) => (theme.isDesktop ? "flex" : "none")};
`;

const Logo = styled.img`
  position: relative;
  top: 30px;
  max-width: 180px;
  height: auto;
  z-index: 101;
  ${({ theme }) =>
    !theme.isLandscape &&
    `
  max-width: 33%;
  top: 1px;
  left: 7%;
  `}
`;

const OpenLinksButton = styled.button`
  width: 70px;
  height: 50px;
  background: none;
  border: none;
  color: white;
  font-size: 45px;
  cursor: pointer;

  @media (min-width: 666px) {
    display: none;
  }
`;

const Drops = styled.div`
  background-image: url(${drippingHoney});
  background-position: center -78px;
  background-repeat: no-repeat;
  background-size: cover;
  width: 100%;
  min-height: 36vh;
  position: relative;

  ${({ theme }) =>
    theme.isBigDisplay &&
    `
    background-position-y: -130px;
    min-height: 43vh;
  `}
`;
