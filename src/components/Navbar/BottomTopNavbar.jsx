import styled from "styled-components";
import SvgWrapper from "../SvgWrapper";

const BottomTopNavbar = () => {
  return (
    <NavbarContainer>
      <SvgWrapper color={"#396140"} />
    </NavbarContainer>
  );
};
export default BottomTopNavbar;

const NavbarContainer = styled.nav`
  display: none;
  width: 100%;
  background-color: black;
  justify-content: center;
  text-align: center;

  @media (max-width: 1360px) {
    height: 40px;
    display: flex;
  }
`;
