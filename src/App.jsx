import { useWindowSize } from "./utils";
import { NavBar, TopNavbar } from "./components";
import { ThemeProvider } from "styled-components";

import "./index.css";

function App() {
  const theme = useWindowSize();

  return (
    <ThemeProvider theme={theme}>
      <TopNavbar />
      <NavBar />
      {/* <BottomTopNavbar /> */}
    </ThemeProvider>
  );
}

export default App;
