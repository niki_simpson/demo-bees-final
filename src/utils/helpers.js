/* eslint-disable no-undef */

export const scrollToTop = () => {
  window.scrollTo({
    top: 0,
    behavior: 'smooth',
    /* you can also use 'auto' behaviour
       in place of 'smooth' */
  });
};

export const handleClickScroll = () => {
  const element = document.getElementById('locations');
  if (element) {
    // 👇 Will scroll smoothly to the top of the next section
    element.scrollIntoView({ behavior: 'smooth' });
  }
};
