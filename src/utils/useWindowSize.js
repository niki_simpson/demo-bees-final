/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-undef */
import { useEffect, useState } from "react";

const useWindowSize = (
  designWindowWidth = 375, //width of entire screen in XD
  designWindowHeight = 812 //height of entire screen in XD
) => {
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
    scaleCoef: 1,
    isLandscape:
      window.innerWidth > window.innerHeight ||
      (window.innerWidth > 1365 && window.innerHeight > 767),
    isDesktop: window.innerWidth > 1365 && window.innerHeight > 767,
  });

  useEffect(() => {
    window.addEventListener("resize", handleResize);
    handleResize();
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  function handleResize() {
    const isDesktop = window.innerWidth > 1365 && window.innerHeight > 767;
    const isBig = window.innerWidth > 1600 && window.innerHeight > 800;
    const isIphoneSE =
      (window.innerWidth === 375 && window.innerHeight === 667) ||
      (window.innerWidth === 667 && window.innerHeight === 375);
    const isLandscape =
      isDesktop || window.innerWidth > window.innerHeight ? true : false;
    const maxWidth = isDesktop ? 9999999 : isLandscape ? 1024 : 480;
    const maxHeight = isDesktop ? 9999999 : isLandscape ? 480 : 1024;
    const designWidth = isDesktop ? 1366 : designWindowWidth;
    const designHeight = isDesktop ? 768 : designWindowHeight;
    const userAgent = navigator.userAgent.toLowerCase();
    const isTablet =
      /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(
        userAgent
      );
    const coef = Math.min(
      (window.innerWidth > maxWidth ? maxWidth : window.innerWidth) /
        designWidth,
      (window.innerHeight > maxHeight ? maxHeight : window.innerHeight) /
        designHeight
    );

    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
      scaleCoef: coef,
      isLandscape: isLandscape,
      isDesktop: isDesktop,
      isOnlyLandscape: isLandscape && !isDesktop,
      isTablet: isTablet,
      isBigDisplay: isBig,
      isIphoneSE: isIphoneSE,
    });
  }
  return windowSize;
};

export default useWindowSize;
